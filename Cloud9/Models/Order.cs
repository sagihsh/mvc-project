﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Cloud9.Models
{
    public class Order
    {
        public int ID { get; set; }
        public int userID { get; set; }
        public User user { get; set; }
        public DateTime purchasedAt { get; set; }
        public int planeId { get; set; }
        public ICollection<Plane> planes { get; set; }
    }
}
