﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Cloud9.Models
{
    public class Plane
    {
        public int ID { get; set; }
        [Required]
        public string color { get; set; }
        public int typeID { get; set; }
        public PlaneType type { get; set; }
        public int price { get; set; }
        public int range { get; set; }
        public SeatMap seatMap { get; set; }
        public int seatMapId { get; set; }
        public ICollection<Order> orders { get; set; }

    }
}
