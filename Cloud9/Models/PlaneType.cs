﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Cloud9.Models
{
    public class PlaneType
    {
        [Key]
        public int ID { get; set; }
        [Required]
        [MaxLength(26, ErrorMessage="A plane type's name must be 3 to 26 characters long"), MinLength(3, ErrorMessage = "A plane type's name must be 3 to 26 characters long")]
        public string name { get; set; }

        public string imageUrl { get; set; }
    }
}