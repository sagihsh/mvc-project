﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Cloud9.Models
{
    public class SeatMap
    {
        [ForeignKey("Plane")]
        public int ID { get; set; }
        public int numSeats { get; set; }

    }
}
