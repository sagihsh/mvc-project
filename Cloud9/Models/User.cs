﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Cloud9.Models
{
    public class User
    {
        [Key]
        public int ID { get; set; }

        [RegularExpression("^[0-9]{9}$", ErrorMessage = "* User ID has to be exactly 9 digits")]
        public string userID { get; set; }
        public string name { get; set; }

        [Required]
        public string email { get; set; }

        [Required]
        [StringLength(30, ErrorMessage = "* Password must be atleast 5 characters", MinimumLength = 5)]
        [DataType(DataType.Password)]
        public string password { get; set; }
        public bool isAdmin { get; set; }
    }
}