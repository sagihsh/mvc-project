﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cloud9.Models;
using Cloud9.Data;
using System.Threading;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Cloud9.Controllers
{
    public class BranchController : Controller
    {
        private Cloud9Context _context;

        public BranchController(Cloud9Context context)
        {
            _context = context;
        }

        // GET: Branch
        public ActionResult Index()
        {
            return View();
        }
        public IActionResult getLocations()
        {
            List<Branch> objLocations = _context.Branches.ToList<Branch>();
            return Json(new { Data = objLocations, success=true });
        }
    }
}
