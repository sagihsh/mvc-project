﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Cloud9.Controllers
{
    public class StatisticsController : Controller
    {
        public IActionResult Index()
        {
            if (HttpContext.User.FindFirst(ClaimTypes.Role).Value == "True")
            {
                return View();
            }
            return View("~/Views/Users/AccessDenied.cshtml");
        }
    }
}
