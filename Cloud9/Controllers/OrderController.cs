﻿using System.Collections.Generic;
using System.Linq;
using Cloud9.Models;
using Cloud9.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Security.Claims;
using System;
using System.Data;

namespace Cloud9.Controllers
{
    public class OrderController : Controller
    {
        public readonly Cloud9Context _context;

        public OrderController(Cloud9Context context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            var orders =  _context.Orders.Include(o => o.user);
            if (HttpContext.User.FindFirst(ClaimTypes.Role).Value != "True")
            {
                return View(_context.Orders.Include(o => o.user).Where
                    (o => o.userID == Int16.Parse(HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value
)).ToList());
            }
            return View(orders);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Order order = await _context.Orders.Include(o => o.user).FirstOrDefaultAsync(o => o.ID == id);

            var planes = _context.Orders.Select(order => new
            {
                order.ID,
                Planes = order.planes.Select(plane => plane.ID).ToList()
            }
            ).Where(order => order.ID.Equals(id)).ToList();

            var newPlanes = new List<Plane>();
            foreach (var planeID in planes[0].Planes)
            {
                newPlanes.Add(_context.Planes.Include(p => p.type).FirstOrDefault(o => o.ID == planeID));
            }

            order.planes = newPlanes;
            
            if (order == null)
            {
                return NotFound();
            }

            if (!IsUserHasPermissions(order.userID))
            {
                return View("~/Views/Users/AccessDenied.cshtml");
            }

            return View(order);
        }

        public IActionResult Create()
        {
            ViewData["userID"] = new SelectList(_context.Set<User>(), "ID", "name");
            ViewData["planeID"] = new SelectList(_context.Set<Plane>(), "ID", "color");
            return View();
        }

        // POST: Orders/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("userID, purchasedAt, planeId")] Order order)
        {
            if (ModelState.IsValid)
            {
                _context.Add(order);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["userID"] = new SelectList(_context.Set<User>(), "ID", "ID", order.userID);
            ViewData["planeId"] = new SelectList(_context.Set<Plane>(), "ID", "ID", order.planeId);
            return View(order);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Orders.FindAsync(id);

            var planes = _context.Orders.Select(order => new
            {
                order.ID,
                Planes = order.planes.Select(plane => plane.ID).ToList()
            }
            ).Where(order => order.ID.Equals(id)).ToList();

            var newPlanes = new List<Plane>();
            foreach (var planeID in planes[0].Planes)
            {
                newPlanes.Add(_context.Planes.Include(p => p.type).FirstOrDefault(o => o.ID == planeID));
            }

            order.planes = newPlanes;

            var planeList = new List<SelectListItem> { };

            foreach (var plane in _context.Planes.Include(p => p.type).ToList())
            {
                    planeList.Add(new SelectListItem
                {
                    Value = plane.ID.ToString(),
                    Text = plane.type.name,
                    Selected = planes[0].Planes.Contains(plane.ID)
                });
            }

            ViewBag.Planes = planeList;


            if (order == null)
            {
                return NotFound();
            }

            if (!IsUserHasPermissions(order.userID))
            {
                return View("~/Views/Users/AccessDenied.cshtml");
            }

            ViewData["userID"] = new SelectList(_context.Set<User>(), "ID", "ID", order.userID);
            ViewData["planeId"] = new SelectList(_context.Set<Plane>(), "ID", "ID", order.planeId);
            return View(order);
        }

        // POST: Orders/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, int [] planes)
        {
            Order order = _context.Orders.Where(o => o.ID == id).FirstOrDefault();
            if (order == null)
            {
                return NotFound();
            }

            var currentPlaneIds = _context.Orders.Select(order => new
            {
                order.ID,
                Planes = order.planes.Select(plane => plane.ID).ToList()
            }
            ).Where(order => order.ID.Equals(id)).ToList();

            planes = planes.Where(x => !currentPlaneIds[0].Planes.Contains(x)).ToArray();

            var newPlanes = new List<Plane>();
            foreach (var planeID in planes)
            {
                newPlanes.Add(_context.Planes.Include(p => p.type).FirstOrDefault(o => o.ID == planeID));
            }

            order.planes = newPlanes;
            _context.Update(order);
            await _context.SaveChangesAsync();

            return RedirectToRoute("Default", new { controller = "Order", action = "Index" });
        }

        // GET: Orders/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Order order = await _context.Orders.Include(o => o.user).FirstOrDefaultAsync(o => o.ID == id);

            if (!IsUserHasPermissions(order.userID))
            {
                return View("~/Views/Users/AccessDenied.cshtml");
            }
            var planes = _context.Orders.Select(order => new
            {
                order.ID,
                Planes = order.planes.Select(plane => plane.ID).ToList()
            }
            ).Where(order => order.ID.Equals(id)).ToList();

            var newPlanes = new List<Plane>();
            foreach (var planeID in planes[0].Planes)
            {
                newPlanes.Add(_context.Planes.Include(p => p.type).FirstOrDefault(o => o.ID == planeID));
            }

            order.planes = newPlanes;

            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }

        // POST: Orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var order = await _context.Orders.FindAsync(id);
            _context.Orders.Remove(order);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool OrderExists(int id)
        {
            return _context.Orders.Any(p => p.ID == id);
        }

        private bool IsUserHasPermissions(int orderUserID)
        {
            return (orderUserID == Int16.Parse(HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value))
                || HttpContext.User.FindFirst(ClaimTypes.Role).Value == "True";
        }

        public IActionResult getOrderCountByUser()
        {
            return Json(new
            {
                Data = _context.Orders
                .GroupBy(o => o.user.name).Select
                (o => new { label = o.Key, value = o.Count() }).ToList(),
                success = true
            });
        }

    }
}


