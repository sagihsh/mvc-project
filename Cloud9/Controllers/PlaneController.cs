﻿using System.Collections.Generic;
using System.Linq;
using Cloud9.Models;
using Cloud9;
using Cloud9.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
//using NPOI.SS.Formula.Functions;

namespace Cloud9.Controllers
{
    public class PlaneController : Controller
    {
        private readonly Cloud9Context _context;

        public PlaneController(Cloud9Context context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            var planesContext = _context.Planes.Include(a => a.type).Include(a => a.seatMap);
            return View(await planesContext.ToListAsync());
        }


        public async Task<IActionResult> Details(int? id)
        {
            Plane plane = await _context.Planes.Include(p => p.type).Include(a => a.seatMap).FirstOrDefaultAsync(p => p.ID == id);
            return View(plane);
        }


        [Authorize(Roles = "True")]
        public IActionResult Create()
        {
            ViewData["typeID"] = new SelectList(_context.Set<PlaneType>(), "ID", "name");
            return View();
        }


        // POST: Planes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "True")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("color, typeID, price, range, seatMapId")] Plane plane)
        {
            if (ModelState.IsValid)
            {
                var newSeatMap = new SeatMap() { numSeats = plane.seatMapId };
                _context.SeatMaps.Add(newSeatMap);
                plane.seatMapId = newSeatMap.ID;
                plane.seatMap = newSeatMap;
                _context.Add(plane);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["typeID"] = new SelectList(_context.Set<PlaneType>(), "ID", "name", plane.type);
            return View(plane);
        }


        [Authorize(Roles = "True")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plane = await _context.Planes.FindAsync(id);
            if (plane == null)
            {
                return NotFound();
            }

            var seatMapList = new List<SelectListItem> { };
            foreach (var seatMap in _context.SeatMaps.ToList())
            {
                seatMapList.Add(new SelectListItem
                {
                    Value = seatMap.ID.ToString(),
                    Text = seatMap.numSeats.ToString(),
                });
            }

            ViewBag.SeatMap = seatMapList;
            ViewData["typeID"] = new SelectList(_context.Set<PlaneType>(), "ID", "name", plane.type);
           
            return View(plane);
        }

        // POST: Planes/Edit/5
        [Authorize(Roles = "True")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID, color, typeID, price, range, seatMapId")] Plane plane)
        {
            if (id != plane.ID)
            {
                return NotFound();
            }

            if (HttpContext.User.FindFirst(ClaimTypes.Role).Value != "True")
            {
                return View("~/Views/Users/AccessDenied.cshtml");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(plane);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PlaneExists(plane.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["typeID"] = new SelectList(_context.Set<PlaneType>(), "ID", "name", plane.type);
            ViewData["seatMapId"] = new SelectList(_context.Set<PlaneType>(), "ID", "numSeats", plane.seatMap);
            return View(plane);
        }

        [Authorize(Roles = "True")]
        // GET: Planes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            if (HttpContext.User.FindFirst(ClaimTypes.Role).Value != "True")
            {
                return View("~/Views/Users/AccessDenied.cshtml");
            }

            var plane = await _context.Planes
                .Include(a => a.type)
                .Include(a => a.seatMap)
                .FirstOrDefaultAsync(p => p.ID == id);
            if (plane == null)
            {
                return NotFound();
            }

            return View(plane);
        }

        // POST: Planes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var plane = await _context.Planes.FindAsync(id);
            _context.Planes.Remove(plane);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PlaneExists(int id)
        {
            return _context.Planes.Any(p => p.ID == id);
        }

        [Authorize]
        public async Task<IActionResult> Buy(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plane = await _context.Planes
                .Include(a => a.type)
                .Include(a => a.seatMap)
                .FirstOrDefaultAsync(p => p.ID == id);
            if (plane == null)
            {
                return NotFound();
            }

            return View(plane);
        }

        //POST: Planes/Buy/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Buy(int id, Plane plane)
        {   
            if (id != plane.ID)
            {
                return NotFound();
            }
            var newPlane = new List<Plane>();
            newPlane.Add(_context.Planes.Include(p => p.type).FirstOrDefault(o => o.ID == plane.ID));

            Order newOrder = new Order { userID = Int16.Parse(HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value)
                , purchasedAt = DateTime.Now,
                planeId = plane.ID};


            newOrder.planes = newPlane;
            _context.Orders.Add(newOrder);
            await _context.SaveChangesAsync();
            return RedirectToRoute("Default", new { controller = "Order", action = "Index" });
        }


        public IActionResult getPlaneColors()
        {
            return Json(new { Data = _context.Planes
                .GroupBy(p => p.color).Select
                (p => new { label = p.Key, value = p.Count() }).ToList(), success = true });
        }

       // [HttpPost]
        //POST: Plane/SerachPlanes
        public IActionResult SearchPlanes(string type, string price, string color)
        {
            var serachResults = _context.Planes.Include(plane => plane.type).Include(a => a.seatMap)
                    .Where(plane => (string.IsNullOrEmpty(type) || plane.type.name.Contains(type))
            && (string.IsNullOrEmpty(color) || plane.color.Contains(color))
            && (string.IsNullOrEmpty(price) || plane.price == Int16.Parse(price))).ToList();

            return Json(new { data = serachResults, success = true });
        }
    }
}