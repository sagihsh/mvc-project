﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cloud9.Data;
using Cloud9.Models;

namespace Cloud9.Data
{
    public class DbInitializer
    {
        public static void Initialize(Cloud9Context context)
        {
            context.Database.EnsureCreated();

            if (context.Planes.Any())
            {
                return;
            }

            // Plane Types Data
            var planeTypes = new PlaneType[]
            {
                new PlaneType { name="Boeing 777-900", imageUrl="https://www.lufthansagroup.com/microsites/_processed_/e/3/csm_LH_BOEING_777_9_AERIAL_VTP_UPDATE_FRONT_THREE_QUARTER_MAY_18_e66e2c9b09.jpg" },
                new PlaneType { name="Airbus A320-300", imageUrl="https://airbus-h.assetsadobe2.com/is/image/content/dam/products-and-solutions/commercial-aircraft/a220-family/a220-300/Airbus-A220-300-new-member-of-the-airbus-Single-aisle-Family.jpg?wid=1920&fit=fit,1&qlt=85,0"},
                new PlaneType { name="Bombardier Q400", imageUrl="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a1/Airberlin_Q400_%28cropped%29.jpg/1200px-Airberlin_Q400_%28cropped%29.jpg"},
                new PlaneType { name="Boeing Dreamlifter", imageUrl="https://i.insider.com/5e820fe01378e33bb151f4ef?width=1200&format=jpeg"},
                new PlaneType { name="Hawker 400", imageUrl="https://lh3.googleusercontent.com/proxy/rTFiDXyZkD89lq3A-vS4jJsar-QVBPajRQEIaHuiAU0YK6XTiwfIPegrHEXQB2SAxS7AiRKGYEPnaR7D_4VQPLJwqMmIF4qYmUFdCWjvAs4Sv4n_t3MSe4MywwQP-N2mXzM"},
                new PlaneType { name="Airbus Beluga XL", imageUrl="https://airbus-h.assetsadobe2.com/is/image/content/dam/products-and-solutions/commercial-aircraft/beluga/belugaxl/BelugaXL.jpg?wid=1920&fit=fit,1&qlt=85,0"},
                new PlaneType { name="Embraer 175", imageUrl="https://content.delta.com/content/www/us/en/aircraft/embraer/erj-175.damAssetRender.20181213T1244472250500.html/content/dam/delta-www/responsive/airports-aircraft/Embraer/Profile-Detail/aircraft-boeing-E175-profile-detail-924.png"}
            };
            foreach (PlaneType pt in planeTypes)
            {
                context.PlaneTypes.Add(pt);
            }
            context.SaveChanges();

            // SeatMaps Data
            var SeatMaps = new SeatMap[]
            {
                new SeatMap { numSeats=100 },
                new SeatMap { numSeats=200 },
                new SeatMap { numSeats=300 },
                new SeatMap { numSeats=400 },
                new SeatMap { numSeats=500 },
                new SeatMap { numSeats=600 },
                new SeatMap { numSeats=700 },

            };
            foreach (SeatMap st in SeatMaps)
            {
                context.SeatMaps.Add(st);
            }
            context.SaveChanges();


            // Planes Data
            var planes = new Plane[]
            {
                new Plane { type=planeTypes[0], color="Blue", price=2600, range=1000, seatMap=SeatMaps[0]},
                new Plane { type=planeTypes[1], color="Yellow", price=5200, range=2000, seatMap=SeatMaps[1] },
                new Plane { type=planeTypes[2], color="Yellow", price=6300, range=1500, seatMap=SeatMaps[2] },
                new Plane { type=planeTypes[3], color="Green", price=6800, range=1700, seatMap=SeatMaps[3] },
                new Plane { type=planeTypes[4], color="Yellow", price=6300, range=1500, seatMap=SeatMaps[4] },
                new Plane { type=planeTypes[5], color="Blue", price=7000, range=1500, seatMap=SeatMaps[5] },
                new Plane { type=planeTypes[6], color="Red", price=6500, range=1550, seatMap=SeatMaps[6]}


            };
            foreach (Plane p in planes)
            {
                context.Planes.Add(p);
            }
            context.SaveChanges();

            // Users Data
            var users = new User[]
            {
                new User { userID="206432908", name="Sagi Haroosh", email="sagitest@gmail.com", password="sagitest", isAdmin=true },
                new User { userID="207458324", name="Dor Cohen", email="dortest@gmail.com", password="dortest", isAdmin=false },
                new User { userID="318850708", name="Itay Benami", email="benamitest@gmail.com", password="benamitest", isAdmin=false }
            };
            foreach (User u in users)
            {
                context.Users.Add(u);
            }
            context.SaveChanges();

            // Orders Data
            var orders = new Order[]
            {
                new Order { user=users[0], purchasedAt=DateTime.ParseExact("26/06/1999", "dd/MM/yyyy", null), planes= new List<Plane>{ planes[0], planes[1]} },
                new Order { user=users[2], purchasedAt=DateTime.ParseExact("25/02/1999", "dd/MM/yyyy", null), planes=new List<Plane>{ planes[0], planes[2]}  }
            };
            foreach (Order p in orders)
            {
                context.Orders.Add(p);
            }
            context.SaveChanges();

            var branches = new Branch[]
{
                new Branch { title="Runway", description="Our largest hangar and storage space.", coordinates="21.9902334743921, 44.901793742900196"},
                new Branch { title="Logistics", description="Customer support headquarters.", coordinates="30.98975860285253, 34.90955673376543"},
                new Branch { title="Offices", description="Our bussiness headquarters.", coordinates="12.162652373785384, 14.84482949661658"},
                new Branch { title="Storage and maintenance", description="Supplying refuels and maintenance", coordinates="60.80359745534358, 80.6581668121291"}
};
            foreach (Branch b in branches)
            {
                context.Branches.Add(b);
            }
            context.SaveChanges();



            //// OrderPlanes Data
            //var orderPlanes = new OrderPlane[]
            //{
            //    new OrderPlane { planeID=planes[0].ID, orderID=orders[0].ID },
            //    new OrderPlane { planeID=planes[0].ID, orderID=orders[1].ID }
            //};
            //foreach (OrderPlane op in orderPlanes)
            //{
            //    context.OrderPlanes.Add(op);
            //}
            //context.SaveChanges();
        }
    }
}
