﻿using Cloud9.Models;
using Microsoft.EntityFrameworkCore;

namespace Cloud9.Data
{
    public class Cloud9Context : DbContext
    {
        public Cloud9Context(DbContextOptions<Cloud9Context> options) : base(options)
        {
        }

        public DbSet<Order> Orders { get; set; }
        public DbSet<Plane> Planes { get; set; }
        public DbSet<PlaneType> PlaneTypes { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Branch> Branches { get; set; }
        public DbSet<SeatMap> SeatMaps { get; set; }
    }
}
