﻿function searchPlanes() {

    var SearchByType = $("#searchType").val().trim();
    var SearchByPrice = $("#searchPrice").val();
    var SearchByColor = $("#searchColor").val().trim();

    var params = {
        type: SearchByType,
        price: SearchByPrice,
        color: SearchByColor
    }

    var data = JSON.stringify(params)
    
    $.ajax({
        type: "GET",
        url: "/Plane/SearchPlanes",
        data: params,
        success: function (results) {   
            console.log(results)
            var planes = results.data;
            var planesDiv = $("#planesControl");
            $("#planesControl").empty();

            if (planes.length == 0 || planes == null) {
                planesDiv.append("<div class='text-danger'>No planes where found</div>")
            }
            else {
                for (i = 0; i < planes.length; i++) {
                    if (planes[i] != null) {
                        var newCrad = `<div class="col-4">
                            <div class="card" style = "width: 18rem;" >

                                <img class="card-img-top" src="${planes[i].type.imageUrl}" alt = "Card image cap" >
                       
                        <div class="card-body">
                            <h5 class="card-title">
                                            ${planes[i].type.name}
                            </h5>
                        </div>
                        <ul class="list-group list-group-flush">

                            <li class="list-group-item">
                                <b>
                                                color:
                                </b>
                                ${planes[i].color}
                            </li>

                            <li class="list-group-item">
                                <b> price: </b>
                                ${planes[i].price}

                            </li>
                            <li class="list-group-item">
                                <b> seats: </b>
                                ${planes[i].seatMap.numSeats}
                            </li>
                        </ul>
                        <div class="card-body">
                            <a href="/Plane/Details/${planes[i].id}" class="btn btn-outline-primary">
                                            Details <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-info-circle-fill" viewBox="0 0 16 16">
                                    <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412-1 4.705c-.07.34.029.533.304.533.194 0 .487-.07.686-.246l-.088.416c-.287.346-.92.598-1.465.598-.703 0-1.002-.422-.808-1.319l.738-3.468c.064-.293.006-.399-.287-.47l-.451-.081.082-.381 2.29-.287zM8 5.5a1 1 0 1 1 0-2 1 1 0 0 1 0 2z" />
                                </svg>
                            </a> &nbsp;
                            <a href="/Plane/Buy/${planes[i].id}" class="btn btn-outline-success">
                                            Buy <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-cart-check-fill" viewBox="0 0 16 16">
                                    <path d="M.5 1a.5.5 0 0 0 0 1h1.11l.401 1.607 1.498 7.985A.5.5 0 0 0 4 12h1a2 2 0 1 0 0 4 2 2 0 0 0 0-4h7a2 2 0 1 0 0 4 2 2 0 0 0 0-4h1a.5.5 0 0 0 .491-.408l1.5-8A.5.5 0 0 0 14.5 3H2.89l-.405-1.621A.5.5 0 0 0 2 1H.5zM6 14a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm7 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm-1.646-7.646-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L8 8.293l2.646-2.647a.5.5 0 0 1 .708.708z" />
                                </svg>
                            </a>&nbsp;
                        </div>
                    </div >
                </div >`;
                        planesDiv.append(newCrad);

                    }
                }
            }

            resetFormSearch()
        },
        error: function (data) { console.log(data); }
    });
}


function resetFormSearch() {
    $("#searchType").val("");
    $("#searchPrice").val("");
    $("#searchColor").val("");
}