﻿function updateOrderedPlanes() {
    var planeIds = $('#select-planes').val();
    var orderNum = document.getElementById("orderNum").innerHTML;
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Order/Edit/" + orderNum,
        data: {
            "planes": planeIds
        },
        success: function (response) {
            handleData(response.data)
        },
        error: function (data) { console.log(data); }
    });
}