﻿function getData() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/Branch/getLocations",
        success: function (response) { createBranchesMap(response.data); },
        error: function (data) { console.log(data); }
    });
}

function createBranchesMap(locations) {
    console.log(locations);
    var map = new Microsoft.Maps.Map('#myMap');

    locations.map((location, i) => {
        let locationCoordinatesArr = location.coordinates.split(', ')
        var locationPosition = map.tryPixelToLocation(new Microsoft.Maps.Point(parseFloat(locationCoordinatesArr[0]),
            parseFloat(locationCoordinatesArr[1])));
        var pushpin = new Microsoft.Maps.Pushpin(locationPosition, {
            title: location.title,
            text: 'i'
        });
        var infobox = new Microsoft.Maps.Infobox(locationPosition, { title: location.title, description: location.description, visible: false });
        infobox.setMap(map);
        Microsoft.Maps.Events.addHandler(pushpin, 'click', function () {
            infobox.setOptions({ visible: true });
        });
        map.entities.push(pushpin);
    })
}
